# Analytics

Please see the derivation here:

[page 1](derivation_1.jpeg)

[page 2](derivation_2.jpeg)

Tests are in `AnalyticsTest`

## Additional Notes

My answers for exercise 2 are slightly off - for the amount of bonds B and C to purchase, I get `283.0606107` and `12.1024106`. These values are off by `0.00001%` and `0.00003%`, representing a price difference of `$0.3025` and `$0.047`, respectively.

If I derived the solution correctly, this discrepancy could be explained if you and I used different software versions and a different rounding algorithm is being called under the hood.

As mentioned in the hand written notes, there's a more complex derivation taking into account the change in account size. My original assumption was that this would need to be taken into consideration (otherwise, where are the funds to purchase securities coming from?). However, I tried the simpler derivation first, and found that I got the aforementioned answers that differ in the last few decimal places from the provided solution. Given the size of the error, I suspect that it's safe to assume that the account size is static.
