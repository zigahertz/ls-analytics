defmodule Analytics do
  alias Data
  require Logger

  @doc """
  returns tuple
  {adjusted_percentage, adjusted_bond_quantity}
  """
  @spec adjust_target_quantity(account :: map(), security :: map(), investment_amount :: float()) ::
          {float(), float()}
  def adjust_target_quantity(account, security, investment_amount \\ 0.0025) do
    adj_percentage =
      Enum.min([investment_amount, Data.issue_limit() - percentage_held(account, security)])

    {r(adj_percentage * 100), bond_quantity(account, security, adj_percentage) |> r()}
  end

  @spec percentage_held(account :: map(), security :: map()) :: float()
  defp percentage_held(%{currently_held: current, size: size}, %{unit_price: price}) do
    current * price / size
  end

  @spec bond_quantity(account :: map(), security :: map(), percentage :: float()) :: float()
  defp bond_quantity(%{size: size}, %{unit_price: price}, percentage) do
    percentage * size / price
  end

  defp r(x, n \\ 2), do: Float.round(x, n)

  @spec neutral_purchase(list(), float()) :: list()
  def neutral_purchase(initial_holds, a_purchase_amount) do
    gamma = gamma(initial_holds, a_purchase_amount)

    [
      b: r(Keyword.get(initial_holds, :b) - beta(initial_holds, a_purchase_amount, gamma), 7),
      c: r(Keyword.get(initial_holds, :c) - gamma, 7)
    ]
  end

  @spec x(list()) :: float()
  defp x(holdings) do
    holdings
    |> Enum.reduce(0, &sum_sensitivity(&1, &2, :x))
    |> r(8)
  end

  @spec y(list()) :: float()
  defp y(holdings) do
    holdings
    |> Enum.reduce(0, &sum_sensitivity(&1, &2, :y))
    |> r(8)
  end

  @spec sum_sensitivity(tuple(), float(), atom()) :: float()
  defp sum_sensitivity({sec, quantity}, sum, key) do
    %{unit_price: price} = security = apply(Data, sec, [])
    sensitivity = Map.get(security, key)

    sum + price * sensitivity * quantity
  end

  @spec beta(list(), float(), float()) :: float()
  defp beta(holdings, purchase_amount, gamma) do
    %{x: x_a, unit_price: p_a} = Data.a()
    %{x: x_b, unit_price: p_b} = Data.b()
    %{x: x_c, unit_price: p_c} = Data.c()

    (x(holdings) - purchase_amount * p_a * x_a - gamma * p_c * x_c) / (x_b * p_b)
  end

  @spec gamma(list(), float()) :: float()
  defp gamma(holdings, purchase_amount) do
    %{x: x_a, y: y_a, unit_price: p_a} = Data.a()
    %{x: x_b, y: y_b} = Data.b()
    %{x: x_c, y: y_c, unit_price: p_c} = Data.c()
    h0 = (y_b / x_b) |> r(8)

    (y(holdings) - h0 * x(holdings) - purchase_amount * p_a * (y_a - h0 * x_a)) /
      r(p_c * (y_c - h0 * x_c), 8)
  end
end
