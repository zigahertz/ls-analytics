defmodule Data do
  def issue_limit, do: 0.01

  def securities do
    [
      {:a, a()},
      {:b, b()},
      {:c, c()}
    ]
  end

  def accounts do
    [
      {:abc, abc()},
      {:bcd, bcd()},
      {:cde, cde()}
    ]
  end

  def a do
    %{
      unit_price: 10_643.95,
      x: 4.444468,
      y: 0.658931555
    }
  end

  def b do
    %{
      unit_price: 10_185.25,
      x: 2.811336,
      y: 0.375198159
    }
  end

  def c do
    %{
      unit_price: 10369.32,
      x: 17.591757,
      y: 3.564
    }
  end

  def abc do
    %{
      size: 45_489_583.23,
      currently_held: 25
    }
  end

  def bcd do
    %{
      size: 217_082_174.54,
      currently_held: 146
    }
  end

  def cde do
    %{
      size: 348_782_139.11,
      currently_held: 300
    }
  end
end
