defmodule AnalyticsTest do
  use ExUnit.Case
  alias Data
  import Analytics

  describe "exercise one" do
    test "adjust_target_quantity/3" do
      assert adjust_target_quantity(Data.abc(), Data.a()) == {0.25, 10.68}
      assert adjust_target_quantity(Data.bcd(), Data.a()) == {0.25, 50.99}
      assert adjust_target_quantity(Data.cde(), Data.a()) == {0.08, 27.68}
    end
  end

  describe "exercise two" do
    assert neutral_purchase([a: 300, b: 23, c: 52], 82) == [
             b: -283.0606404,
             c: -12.1024106
           ]
  end
end
